#!/usr/bin/env python
# coding: utf-8

import bottle

from datetime import datetime
from io import StringIO
from json import dumps, loads, JSONDecodeError
from logging import getLogger, StreamHandler, INFO

from psql import Plugin


app = application = bottle.Bottle()
app.install(
    Plugin(
        "host=postgresql port=5432 dbname=iktos user=iktos password=VurR!V/&C09f7qb-nlZnb}n=<",
        keyword='db'
    )
)
LOGGER = getLogger(__name__)
LOGGER.setLevel(INFO)
sh = StreamHandler()
sh.setLevel(INFO)
LOGGER.addHandler(sh)
# A list of all supported type and ways to check they are valid:
_TYPE_CONVERT = {
    "smallint": int,
    "int": int,
    "integer": int,
    "bigint": int,
    "real": float,
    "double precision": float,
    "smallserial": int,
    "serial": int,
    "bigserial": int,
    "date": datetime.fromisoformat,
    "timestamp": datetime.fromisoformat,
    "text": str,
    "boolean": bool,
}


def schema_exist(db, name):
    """Test if the schema name exist in the database db.

    :param db: Database
    :param name: Schema name.
    :return: True if the schema exist, else False.
    """
    db.execute(f"SELECT name, struct FROM admin_info.schemas WHERE name LIKE '{name}'")
    data = db.fetchall()

    return len(data) > 0


def get_json_from_request(db, name):
    """Handle name conflict for schema and missing json data.

    :param db: Database cursor.
    :param name: Schema name.
    :return: Json attach to the request.
    """
    # Checking if the schema exist:
    if not schema_exist(db, name):
        log_msg = f"Schema '{name}' does not exist."
        LOGGER.error(log_msg)
        bottle.response.status = 409
        return log_msg

    # Trying to extract the body as json:
    struct = bottle.request.json
    if struct is None:
        log_msg = f"Missing JSON data in the request while inserting into '{name}'."
        LOGGER.error(log_msg)
        bottle.response.status = 400
        return log_msg

    # Trying to transform the json string into a python structure:
    try:
        struct = loads(struct)
    except JSONDecodeError as e:
        log_msg = f"JSON data weren't correctly formatted: {e}."
        LOGGER.error(log_msg)
        bottle.response.status = 400
        return log_msg

    return struct


def get_csv_from_request(db, name):
    """Handle name conflict for schema and missing json data.

    :param db: Database.
    :param name: Schema name.
    :return: csv attach to the request.
    """
    # Checking if the schema exist:
    if not schema_exist(db, name):
        log_msg = f"Schema '{name}' does not exist."
        LOGGER.error(log_msg)
        bottle.response.status = 409
        return log_msg

    # Check we did get a body:
    body = bottle.request.body
    if body is None:
        log_msg = f"Missing CSV data in the request while inserting into '{name}' (no body)."
        LOGGER.error(log_msg)
        bottle.response.status = 400
        return log_msg

    # Get a string from the byte object:
    buf = body.getvalue().decode()
    if buf == '':
        log_msg = f"Missing CSV data in the request while inserting into '{name}' (empty body)."
        LOGGER.error(log_msg)
        bottle.response.status = 400
        return log_msg

    data = list()
    for line in buf.split('\n'):
        # Removing all extra spaces at start and end of the line:
        line = line.strip()

        # Ignore empty line:
        if line == '':
            continue
        # Line starting with a # are comments:
        if line[0] == '#':
            continue

        # Split the line on each ',':
        line = line.split(',')

        # Adding it to our list:
        data.append([l.strip() for l in line])

    # for the sake of factorisation, transform the list into a list of dictionary:
    return [{k: v for k, v in zip(data[0], line)} for line in data[1:]]


def json_to_sql_table(parsed_json):
    """Transform the json describing a schema structure into a sql declaration for the CREATE TABLE request.

    :param parsed_json: JSON to transform.
    :return: SQL string used to declare the column of a table.
    """
    return ", ".join([f"{k} {v}" for k, v in parsed_json.items()])


def dictrow_to_csv(columns, data):
    """Transform the DictRow object returned by the SQL request into a csv string.

    :param columns: Columns of the structure.
    :param data: Data to transform.
    :return: A string containing the CSV.
    """
    with StringIO() as sio:
        sio.write(",".join(columns))
        sio.write("\n")

        for line in data:
            sio.write(
                ",".join([str(line[col]) for col in columns]) + "\n"
            )

        return sio.getvalue()


def dictrow_to_json(data):
    """Transform the DictRow object returned by the SQL request into a JSON string.

    :param data: Data to transform.
    :return: A string containing the JSON.
    """
    output = list()
    for line in data:
        output.append(
            {k: str(v) for k, v in line.items()}
        )

    return dumps(output)


def clean_up(db, name, data):
    """Clean up data from line missing information.

    :param db: Database.
    :param name: Table name.
    :param data: Data to filter.
    :return: Filtered data.
    """
    # First, lets get column informations:
    db.execute(f"SELECT struct FROM admin_info.schemas WHERE name LIKE '{name}'")
    colinfo = loads(db.fetchone()["struct"])
    columns = [ col for col in colinfo.keys() ]
    orig_size = len(data)

    # First stage, remove line without enough column:
    data = filter(
        lambda x: len(x) == len(columns),
        data
    )

    out = list()
    for line in data:
        # Remove lines with empty field:
        line = dict(filter(lambda x: x[1] != "", line.items()))
        if len(line) != len(columns):
            continue

        # Checking type validity:
        for name, typ in colinfo.items():
            try:
                _TYPE_CONVERT[typ](line[name])
            except ValueError as e:
                line.pop(name)
        if len(line) != len(columns):
            continue

        out.append(line)

    return orig_size - len(out), out


def sanitize(data):
    """If the argument is a string, sanitize it. Else return it."""
    if isinstance(data, str):
        return data.replace("'", r"\'")
    return data


def insert_into_db(db, name, data):
    """Insert data into the table of database `db`.

    :param db: Database.
    :param name: table name.
    :param data: data to ingest.
    """
    # First, lets get column informations:
    db.execute(f"SELECT struct FROM admin_info.schemas WHERE name LIKE '{name}'")
    columns = [ col for col in loads(db.fetchone()["struct"]).keys() ]

    LOGGER.debug("Columns: %s.", columns)

    for line in data:
        # Lets build the string for the data:
        data_repr = ', '.join([f"E'{sanitize(line[col])}'" for col in columns])
        sql = f"INSERT INTO {name} ({', '.join(columns)}) VALUES ({data_repr})"

        LOGGER.info(sql)

        # In the case of psycopg2, using execute in a loop or executemany is
        # the same performance wise. Hope it gets better in psycopg3.
        db.execute(sql)


@app.error(500)
def error(error):
    """Deal with any internal error that were not handled.

    :param error: The error that happened.
    :return: A string of the error.
    """
    return f"An error occurred: {error}."


@app.route('/<name>', method='POST')
def schema(db, name):
    """Create schema name.

    :param db: Database to use.
    :param name: Name of the schema.
    :return: String if an error happened, else None.
    """
    try:
        LOGGER.info("Getting ready to create schema '%s'.", name)
        # First try if it exist:
        if schema_exist(db, name):
            log_msg = f"Schema '{name}' already exist."
            LOGGER.error(log_msg)
            bottle.response.status = 409
            return log_msg

        json = bottle.request.json
        if json is None:
            log_msg = f"Missing JSON data in the request while inserting into '{name}'."
            LOGGER.error(log_msg)
            bottle.response.status = 400
            return log_msg

        try:
            struct = loads(json)
        except JSONDecodeError as e:
            log_msg = f"JSON data weren't correctly formatted: {e}."
            LOGGER.error(log_msg)
            bottle.response.status = 400
            return log_msg

        db.execute(f"CREATE TABLE {name} ({json_to_sql_table(struct)})")
        db.execute(f"INSERT INTO admin_info.schemas VALUES ('{name}', '{json}')")
        db.execute(f"INSERT INTO admin_info.stats VALUES ('now'::timestamp, '{name}', 'CREATE', '0', '0', '0')")
    except Exception as e:
        bottle.response.status = 500
        db.execute(f"INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'schema', '{name}', '{sanitize(e)}')")
        return str(e)


@app.route('/<name>', method='PUT')
def insert(db, name):
    """Insert data into the schema.

    :param db: Database cursor to ingest into.
    :param name: Schema name.
    :return: String if an error happened, else None.
    """
    try:
        content = bottle.request.get_header('Content-Type')
        LOGGER.info("Getting ready to ingest into schema '%s' using '%s'.", name, content)

        if content == "application/csv":
            # Parse the csv file:
            data = get_csv_from_request(db, name)
        elif "application/json" in content:
            # Parse the json file:
            data = get_json_from_request(db, name)
        else:
            bottle.response.status = 406
            return "Content type not accepted."

        if isinstance(data, str):
            return data
        else:
            original_size = len(data)
            error_count, data = clean_up(db, name, data)
            insert_into_db(db, name, data)

            # Adding ingestion stats into the database:
            db.execute(f"INSERT INTO admin_info.stats VALUES ('now'::timestamp, '{name}', 'PUT', '{original_size}', '{error_count}', '{len(data)}')")
            if error_count > 0:
                return f"There has been {error_count} errors during ingestion."
    except Exception as e:
        bottle.response.status = 500
        db.execute(f"INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'insert', '{name}', '{sanitize(e)}')")
        return str(e)


@app.route('/<name>', method='GET')
def get(db, name):
    """Get all data contained in the schema.

    :param db: Database cursor.
    :param name: Schema name.
    :return: String if an error happened, else None.
    """
    try:
        content = bottle.request.get_header('Content-Type')
        LOGGER.info("Getting ready to ingest into schema '%s' using '%s'.", name, content)

        if not schema_exist(db, name):
            log_msg = f"Schema '{name}' does not exist."
            LOGGER.error(log_msg)
            bottle.response.status = 409
            return log_msg

        # First, lets get column informations:
        db.execute(f"SELECT struct FROM admin_info.schemas WHERE name LIKE '{name}'")
        tmp = db.fetchone()
        LOGGER.info("%s", tmp)
        columns = [col for col in loads(tmp["struct"]).keys()]

        # Extract data:
        db.execute(f"SELECT * FROM {name}")
        data = db.fetchall()

        # Parse them into the wanted format:
        if content == "application/csv":
            bottle.response.set_header('Content-Type', "application/csv")
            return dictrow_to_csv(columns, data)
        elif "application/json" in content:
            bottle.response.set_header('Content-Type', "application/json")
            return dictrow_to_json(data)
        else:
            bottle.response.status = 404
            return "Content type not recognised."
    except Exception as e:
        bottle.response.status = 500
        db.execute(f"INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'get', '{name}', '{sanitize(e)}')")
        return str(e)


@app.route('/<name>', method='DELETE')
def delete(db, name):
    """Remove the given schema from the database.

    :param db: Database cursor.
    :param name: Schema name.
    :return: String if an error happened, else None.
    """
    try:
        if not schema_exist(db, name):
            log_msg = f"Schema '{name}' does not exist."
            LOGGER.error(log_msg)
            bottle.response.status = 409
            return log_msg

        db.execute(f"SELECT COUNT(*) FROM {name}")
        size = db.fetchone()["count"]
        db.execute(f"DROP TABLE IF EXISTS {name} CASCADE;")
        db.execute(f"DELETE FROM admin_info.schemas WHERE name LIKE '{name}'")
        db.execute(f"INSERT INTO admin_info.stats VALUES ('now'::timestamp, '{name}', 'DELETE', '{size}', '0', '0')")
    except Exception as e:
        bottle.response.status = 500
        db.execute(f"INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'delete', '{name}', '{sanitize(e)}')")
        return str(e)


@app.route('/', method='GET')
def show(db):
    """List all available schema in the application.

    :param db: Database cursor.
    :return: String if an error happened, else a JSON with the schema and there structure.
    """
    try:
        db.execute("SELECT name, struct FROM admin_info.schemas")
        return dumps({
            schema["name"]: schema["struct"] for schema in db.fetchall()
        })
    except Exception as e:
        bottle.response.status = 500
        db.execute(f"INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'show', 'NULL', '{sanitize(e)}')")
        return str(e)


if __name__ == "__main__":
    app.run(host='localhost', port=8080, reloader=True)
