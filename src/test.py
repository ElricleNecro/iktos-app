# coding: utf-8

import bottle

from io import BytesIO, StringIO
from json import dumps
from logging import getLogger, FATAL
from unittest import TestCase, main
from unittest.mock import call, Mock, patch

from run import schema_exist, get_json_from_request, get_csv_from_request
from run import json_to_sql_table, dictrow_to_csv, dictrow_to_json, sanitize
from run import insert_into_db, schema, insert, get, delete, show, clean_up, error


LOGGER = getLogger("run")
LOGGER.setLevel(FATAL)


class IktosUnitTest(TestCase):
    """Unit test for the iktos test app."""

    def test_schema_exist_true(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        self.assertTrue(schema_exist(db, "test_full"))
        db.execute.assert_called_once()
        db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_full'")
        db.fetchall.assert_called_once()

    def test_schema_exist_false(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": list()}
        )
        self.assertFalse(schema_exist(db, "test_empty"))
        db.execute.assert_called_once()
        db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
        db.fetchall.assert_called_once()


    def test_get_json_from_request_no_schema(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": list()}
        )
        with patch("bottle.request") as mock:
            mock.json = "{'a': 'b'}"
            ret = get_json_from_request(db, "test_empty")
            self.assertEqual("Schema 'test_empty' does not exist.", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()

    def test_get_json_from_request_no_body(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        with patch("bottle.request") as mock:
            mock.json = None
            ret = get_json_from_request(db, "test_empty")
            self.assertEqual("Missing JSON data in the request while inserting into 'test_empty'.", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()

    def test_get_json_from_request_bad_json(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        with patch("bottle.request") as mock:
            mock.json = "{a: 'b'}"
            ret = get_json_from_request(db, "test_empty")
            self.assertIn("JSON data weren't correctly formatted: ", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()

    def test_get_json_from_request_good(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        with patch("bottle.request") as mock:
            mock.json = '{"a": "b"}'
            ret = get_json_from_request(db, "test_empty")
            self.assertEqual({'a': 'b'}, ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()


    def test_get_csv_from_request_no_schema(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": list()}
        )
        with patch("bottle.request") as mock:
            mock.body = BytesIO("id,name\n1,hoho\n".encode())
            ret = get_csv_from_request(db, "test_empty")
            self.assertEqual("Schema 'test_empty' does not exist.", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()

    def test_get_csv_from_request_no_body(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        with patch("bottle.request") as mock:
            mock.body = None
            ret = get_csv_from_request(db, "test_empty")
            self.assertEqual("Missing CSV data in the request while inserting into 'test_empty' (no body).", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()

    def test_get_csv_from_request_empty_body(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        with patch("bottle.request") as mock:
            mock.body = BytesIO(''.encode())
            ret = get_csv_from_request(db, "test_empty")
            self.assertEqual("Missing CSV data in the request while inserting into 'test_empty' (empty body).", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()

    def test_get_csv_from_request_good(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": [0, 1]}
        )
        with patch("bottle.request") as mock:
            mock.body = BytesIO("id,name\n1,hoho\n #this is a test.\n".encode())
            ret = get_csv_from_request(db, "test_empty")
            self.assertEqual([{"id": "1", "name": "hoho"}], ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test_empty'")
            db.fetchall.assert_called_once()


    def test_json_to_sql_table(self):
        self.assertEqual(
            "id int, name text",
            json_to_sql_table({"id": "int", "name": "text"})
        )

    def test_json_to_sql_table_2(self):
        self.assertEqual(
            "id int",
            json_to_sql_table({"id": "int"})
        )


    def test_dictrow_to_csv(self):
        columns = ["id", "name"]
        data = [{"id": 1, "name": "Antoine"}, {"id": 2, "name": "Ann'sophie"}]
        result = "id,name\n1,Antoine\n2,Ann'sophie\n"

        self.assertEqual(
            result,
            dictrow_to_csv(columns, data)
        )

    def test_dictrow_to_json(self):
        data = [{"id": 1, "name": "Antoine"}, {"id": 2, "name": "Ann'sophie"}]
        result = """[{"id": "1", "name": "Antoine"}, {"id": "2", "name": "Ann'sophie"}]"""

        self.assertEqual(
            result,
            dictrow_to_json(data)
        )


    def test_sanitize_str(self):
        self.assertEqual(
            r"Ann\'sophie",
            sanitize("Ann'sophie")
        )

    def test_sanitize_int(self):
        self.assertEqual(
            10,
            sanitize(10)
        )

    def test_sanitize_float(self):
        self.assertEqual(
            10.,
            sanitize(10.)
        )

    def test_sanitize_date(self):
        self.assertEqual(
            "2020-10-17",
            sanitize("2020-10-17")
        )

    def test_sanitize_bool(self):
        self.assertEqual(
            True,
            sanitize(True)
        )


    def test_insert_into(self):
        db = Mock()
        db.configure_mock(
            **{"fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}}
        )
        data = [
            {"id": 1, "name": "Antoine", "date": "2020-10-17"},
            {"id": 2, "name": "Ann'sophie", "date": "2035-12-25"},
        ]

        self.assertIsNone(insert_into_db(db, "test", data))

        db.execute.assert_has_calls(
            [
                call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
                call("INSERT INTO test (id, name, date) VALUES (E'1', E'Antoine', E'2020-10-17')"),
                call(r"INSERT INTO test (id, name, date) VALUES (E'2', E'Ann\'sophie', E'2035-12-25')"),
            ]
        )
        db.fetchone.assert_called_once()


    def test_schema_not_exist(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": list()}
        )

        with patch("bottle.request") as mock:
            mock.json = """{"id": "int", "name": "text", "date": "date"}"""
            schema(db, "test")

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("CREATE TABLE test (id int, name text, date date)"),
            call("""INSERT INTO admin_info.schemas VALUES ('test', '{"id": "int", "name": "text", "date": "date"}')"""),
            call("INSERT INTO admin_info.stats VALUES ('now'::timestamp, 'test', 'CREATE', '0', '0', '0')"),
        ])
        db.fetchall.assert_called_once()

    def test_schema_bad_json(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": list()}
        )

        with patch("bottle.request") as mock:
            mock.json = """{id: "int", "name": "text", "date": "date"}"""
            ret = schema(db, "test")

        self.assertIn("JSON data weren't correctly formatted: ", ret)
        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
        ])
        db.fetchall.assert_called_once()

    def test_schema_empty(self):
        db = Mock()
        db.configure_mock(
            **{"fetchall.return_value": list()}
        )

        with patch("bottle.request") as mock:
            mock.json = None
            ret = schema(db, "test")

        self.assertEqual("Missing JSON data in the request while inserting into 'test'.", ret)
        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
        ])
        db.fetchall.assert_called_once()

    def test_schema_exist(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchone.return_value": {"struct": {"id": "int", "name": "text", "date": "date"}},
                "fetchall.return_value": [0, 1],
            }
        )

        ret = schema(db, "test")

        self.assertEqual("Schema 'test' already exist.", ret)

    def test_schema_raise(self):
        db = Mock()
        with patch("run.schema_exist") as mock:
            mock.side_effect = Exception('Boom!')
            ret = schema(db, "test")

        self.assertEqual("Boom!", ret)

        db.execute.assert_has_calls([
            call("INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'schema', 'test', 'Boom!')"),
        ])


    def test_insert_csv_good(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = "id,name,date\n1,Antoine,2020-10-17\n2,Ann'sophie,2035-12-25\n"

        with patch("bottle.request") as mock:
            mock.body = BytesIO(data.encode())
            mock.get_header.return_value = "application/csv"
            ret = insert(db, "test")
            self.assertIsNone(ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("INSERT INTO test (id, name, date) VALUES (E'1', E'Antoine', E'2020-10-17')"),
            call(r"INSERT INTO test (id, name, date) VALUES (E'2', E'Ann\'sophie', E'2035-12-25')"),
            call("INSERT INTO admin_info.stats VALUES ('now'::timestamp, 'test', 'PUT', '2', '0', '2')"),
        ])
        db.fetchall.assert_called_once()
        db.fetchone.assert_called()

    def test_insert_csv_good_with_error(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = "id,name,date\n1,Antoine,2020-10-17\n2,Ann'sophie,2035-12-25\n3,Martin,Mauvaise date\n"

        with patch("bottle.request") as mock:
            mock.body = BytesIO(data.encode())
            mock.get_header.return_value = "application/csv"
            ret = insert(db, "test")
            self.assertEqual("There has been 1 errors during ingestion.", ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("INSERT INTO test (id, name, date) VALUES (E'1', E'Antoine', E'2020-10-17')"),
            call(r"INSERT INTO test (id, name, date) VALUES (E'2', E'Ann\'sophie', E'2035-12-25')"),
            call("INSERT INTO admin_info.stats VALUES ('now'::timestamp, 'test', 'PUT', '3', '1', '2')"),
        ])
        db.fetchall.assert_called_once()
        db.fetchone.assert_called()

    def test_insert_csv_no_body(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )

        with patch("bottle.request") as mock:
            mock.body = None
            mock.get_header.return_value = "application/csv"
            ret = insert(db, "test")
            self.assertEqual("Missing CSV data in the request while inserting into 'test' (no body).", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'")
            db.fetchall.assert_called_once()

    def test_insert_csv_empty_body(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )

        with patch("bottle.request") as mock:
            mock.body = BytesIO(''.encode())
            mock.get_header.return_value = "application/csv"
            ret = insert(db, "test")
            self.assertEqual("Missing CSV data in the request while inserting into 'test' (empty body).", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'")
            db.fetchall.assert_called_once()

    def test_insert_csv_no_schema(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": list(),
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )

        with patch("bottle.request") as mock:
            mock.body = None
            mock.get_header.return_value = "application/csv"
            ret = insert(db, "test")
            self.assertEqual("Schema 'test' does not exist.", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'")
            db.fetchall.assert_called_once()

    def test_insert_csv_wrong_content_type(self):
        db = Mock()
        data = "id,name,date\n1,Antoine,2020-10-17\n2,Ann'sophie,2035-12-25\n"

        with patch("bottle.request") as mock:
            mock.body = BytesIO(data.encode())
            mock.get_header.return_value = "application/wrong_content_type"
            ret = insert(db, "test")
            self.assertEqual("Content type not accepted.", ret)

        db.execute.assert_not_called()
        db.fetchall.assert_not_called()
        db.fetchone.assert_not_called()

    def test_insert_raise(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = "id,name,date\n1,Antoine,2020-10-17\n2,Ann'sophie,2035-12-25\n"

        with patch("bottle.request") as req_mock:
            with patch("run.schema_exist") as mock:
                req_mock.body = BytesIO(data.encode())
                req_mock.get_header.return_value = "application/csv"
                mock.side_effect = Exception('Boom!')
                ret = insert(db, "test")

        self.assertEqual("Boom!", ret)

        db.execute.assert_has_calls([
            call("INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'insert', 'test', 'Boom!')"),
        ])


    def test_insert_json_good(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = [
            {"id": 1, "name": "Antoine", "date": "2020-10-17"},
            {"id": 2, "name": "Ann'sophie", "date": "2035-12-25"},
        ]

        with patch("bottle.request") as mock:
            mock.json = dumps(data)
            mock.get_header.return_value = "application/json"
            ret = insert(db, "test")
            self.assertIsNone(ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("INSERT INTO test (id, name, date) VALUES (E'1', E'Antoine', E'2020-10-17')"),
            call(r"INSERT INTO test (id, name, date) VALUES (E'2', E'Ann\'sophie', E'2035-12-25')"),
            call("INSERT INTO admin_info.stats VALUES ('now'::timestamp, 'test', 'PUT', '2', '0', '2')"),
        ])
        db.fetchall.assert_called_once()
        db.fetchone.assert_called()

    def test_insert_json_no_body(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = [
            {"id": 1, "name": "Antoine", "date": "2020-10-17"},
            {"id": 2, "name": "Ann'sophie", "date": "2035-12-25"},
        ]

        with patch("bottle.request") as mock:
            mock.json = None
            mock.get_header.return_value = "application/json"
            ret = insert(db, "test")
            self.assertEqual("Missing JSON data in the request while inserting into 'test'.", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'")
            db.fetchall.assert_called_once()

    def test_insert_json_no_schema(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": list(),
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = "id,name,date\n1,Antoine,2020-10-17\n2,Ann'sophie,2035-12-25\n"

        with patch("bottle.request") as mock:
            mock.json = None
            mock.get_header.return_value = "application/json"
            ret = insert(db, "test")
            self.assertEqual("Schema 'test' does not exist.", ret)
            db.execute.assert_called_once()
            db.execute.assert_called_with("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'")
            db.fetchall.assert_called_once()


    def test_delete_good(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {'count': 2},
            }
        )

        ret = delete(db, "test")
        self.assertIsNone(ret)
        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT COUNT(*) FROM test"),
            call("DROP TABLE IF EXISTS test CASCADE;"),
            call("DELETE FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("INSERT INTO admin_info.stats VALUES ('now'::timestamp, 'test', 'DELETE', '2', '0', '0')")
        ])
        db.fetchall.assert_called_once()

    def test_delete_no_schema(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": list(),
            }
        )

        ret = delete(db, "test")
        self.assertEqual("Schema 'test' does not exist.", ret)
        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
        ])
        db.fetchall.assert_called_once()

    def test_delete_wrong_content_type(self):
        db = Mock()

        with patch("run.schema_exist") as mock:
            mock.side_effect = Exception('Boom!')
            ret = delete(db, "test")

        self.assertEqual("Boom!", ret)
        db.execute.assert_has_calls([
            call("INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'delete', 'test', 'Boom!')"),
        ])


    def test_show(self):
        db = Mock()
        data = [
            {"name": "test", "struct": {'id': "int", "name": "text"}},
            {"name": "source", "struct": {'sourceid': "bigint", "alpha": "double", "delta": "double"}},
        ]
        expected = {
            "test": {'id': "int", "name": "text"},
            "source": {'sourceid': "bigint", "alpha": "double", "delta": "double"},
        }
        db.configure_mock(
            **{"fetchall.return_value": data}
        )

        ret = show(db)
        self.assertEqual(dumps(expected), ret)
        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas"),
        ])
        db.fetchall.assert_called_once()

    def test_show_raise(self):
        db = Mock()
        db.fetchall.side_effect = Exception("Boom!")

        ret = show(db)
        self.assertEqual("Boom!", ret)
        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas"),
            call("INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'show', 'NULL', 'Boom!')"),
        ])
        db.fetchall.assert_called_once()


    def test_get_not_exist(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": list(),
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )

        with patch("bottle.request") as mock:
            mock.get_header.return_value = "application/csv"
            ret = get(db, "test")

        self.assertEqual("Schema 'test' does not exist.", ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
        ])
        db.fetchall.assert_called_once()

    def test_get_csv(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = [
            {"id": 1, "name": "Antoine", "date": "2020-10-17"},
            {"id": 2, "name": "Ann'sophie", "date": "2035-12-25"},
        ]

        with patch("bottle.request") as mock:
            with patch("run.dictrow_to_csv") as dcsv:
                with patch("run.dictrow_to_json") as djson:
                    dcsv.return_value = "In csv"
                    djson.return_value = "In json"
                    mock.get_header.return_value = "application/csv"
                    ret = get(db, "test")

        self.assertEqual("In csv", ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT * FROM test"),
        ])
        db.fetchall.assert_called()
        db.fetchone.assert_called_once()

    def test_get_json(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )
        data = [
            {"id": 1, "name": "Antoine", "date": "2020-10-17"},
            {"id": 2, "name": "Ann'sophie", "date": "2035-12-25"},
        ]

        with patch("bottle.request") as mock:
            with patch("run.dictrow_to_csv") as dcsv:
                with patch("run.dictrow_to_json") as djson:
                    dcsv.return_value = "In csv"
                    djson.return_value = "In json"
                    mock.get_header.return_value = "application/json"
                    ret = get(db, "test")

        self.assertEqual("In json", ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT * FROM test"),
        ])
        db.fetchall.assert_called()
        db.fetchone.assert_called_once()

    def test_get_wrong_content_type(self):
        db = Mock()
        db.configure_mock(
            **{
                "fetchall.return_value": [0, 1],
                "fetchone.return_value": {"struct": '{"id": "int", "name": "text", "date": "date"}'}
            }
        )

        with patch("bottle.request") as mock:
            with patch("run.dictrow_to_csv") as dcsv:
                with patch("run.dictrow_to_json") as djson:
                    dcsv.return_value = "In csv"
                    djson.return_value = "In json"
                    mock.get_header.return_value = "application/wrong_content_type"
                    ret = get(db, "test")

        self.assertEqual("Content type not recognised.", ret)

        db.execute.assert_has_calls([
            call("SELECT name, struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
            call("SELECT * FROM test"),
        ])
        db.fetchall.assert_called()
        db.fetchone.assert_called_once()

    def test_get_raise(self):
        db = Mock()

        with patch("bottle.request") as mock:
            with patch("run.schema_exist") as scex_mock:
                mock.get_header.return_value = "application/csv"
                scex_mock.side_effect = Exception('Boom!')
                ret = get(db, "test")

        self.assertEqual("Boom!", ret)

        db.execute.assert_has_calls([
            call("INSERT INTO admin_info.errors VALUES ('now'::timestamp, 'get', 'test', 'Boom!')"),
        ])


    def test_clean_up_good(self):
        db = Mock()
        db.configure_mock(
            **{"fetchone.return_value": {"struct": '{"id": "int", "name": "text"}'}}
        )

        data = [{"id": 1, "name": "Antoine"}, {"id": 2, "name": "Ann'sophie"}]
        resultat = [{"id": 1, "name": "Antoine"}, {"id": 2, "name": "Ann'sophie"}]

        count, ret = clean_up(db, "test", data)
        self.assertEqual(resultat, ret)
        self.assertEqual(0, count)

        db.execute.assert_has_calls([
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
        ])
        db.fetchone.assert_called_once()

    def test_clean_up_filtered(self):
        db = Mock()
        db.configure_mock(
            **{"fetchone.return_value": {"struct": '{"id": "int", "name": "text"}'}}
        )

        data = [
            {"id": 1, "name": "Antoine"},
            {"id": 2, "name": "Ann'sophie"},
            {"id": 3},
            {"name": ""},
            {"id": 4, "name": ""},
            {"id": "Big apple", "name": ""},
        ]
        resultat = [
            {"id": 1, "name": "Antoine"},
            {"id": 2, "name": "Ann'sophie"},
        ]

        count, ret = clean_up(db, "test", data)
        self.assertEqual(resultat, ret)
        self.assertEqual(4, count)

        db.execute.assert_has_calls([
            call("SELECT struct FROM admin_info.schemas WHERE name LIKE 'test'"),
        ])
        db.fetchone.assert_called_once()


    def test_error(self):
        err = "This is a test error."
        self.assertEqual(f"An error occurred: {err}.", error(err))


if __name__ == "__main__":
    main()
