FROM python:3.8

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt && pip install --no-cache-dir "uwsgi==2.0.19.1"

CMD [ "uwsgi", "./uwsgi.ini" ]
