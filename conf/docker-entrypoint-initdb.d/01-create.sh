#!/bin/bash

set -e

psql --username iktos iktos <<-EOSQL
CREATE SCHEMA IF NOT EXISTS admin_info;
CREATE TABLE IF NOT EXISTS admin_info.schemas (name TEXT PRIMARY KEY, struct TEXT);
CREATE TABLE IF NOT EXISTS admin_info.stats (hour TIMESTAMP WITH TIME ZONE, name TEXT, type TEXT, total BIGINT, erroneous BIGINT, ingested BIGINT);
CREATE TABLE IF NOT EXISTS admin_info.errors (hour TIMESTAMP WITH TIME ZONE, method TEXT, route TEXT, message TEXT);
EOSQL
