Iktos test
==========

[![pipeline status](https://gitlab.com/ElricleNecro/iktos-app/badges/master/pipeline.svg)](https://gitlab.com/ElricleNecro/iktos-app/-/commits/master)
[![coverage report](https://gitlab.com/ElricleNecro/iktos-app/badges/master/coverage.svg)](https://gitlab.com/ElricleNecro/iktos-app/-/commits/master)

## Description
This is a web app using Bottle to create routes created for an interview test. There are 4 routes
implemented:
- `POST /<name>` expects a JSON with the request. It will create the schema `<name>` described in the
  JSON. It should be formatted as follow:
  ```json
  {
    "key1": "type",
    "key2": "type",
    ...
  }
  ```
  The application support a small **subset** of all available SQL types. Those
  are:
  - smallint,
  - int,
  - integer,
  - bigint,
  - real,
  - double precision,
  - smallserial,
  - serial,
  - bigserial,
  - date (ISO 8601 format only),
  - timestamp (ISO 8601 format only),
  - text,
  - boolean.

- `PUT /<name>` expects one of two things:
  - a JSON with the data to be inserted in the schema `<name>`. The JSON must contain a list as
    first element, and below a set dictionary (set of key value pairs) like in the following
    example:
    ```json
    [
      {
        "id": "1",
        "name": "Martin"
      },
      {
        "id": "2",
        "name": "Marie"
      }
    ]
    ```
    will provide two lines to insert:
    1. 1,Martin
    2. 2,Marie
  - a CSV file with the data to insert (the content type must be set to `application/csv`). It is
    formatted as follow:
    ```csv
    column_name1,column_name2
    1,Martin
    2,Marie
    ```
  When processing those data, the application will remove any line with:
  - missing column compared to the schema,
  - lines with column which do not respect the declared type.

- `GET /<name>` will send back data in JSON or CSV depending on the content type of the request. It
  support both `application/csv` and `application/json` content type.

- `GET /` return as JSON a list of all available schema and there structure.

- `DELETE /<name>` remove the schema `<name>`.

## Limitation

There is some limitation to this application. As already mentioned above, it only support a limited
set of data type:
- smallint,
- int,
- integer,
- bigint,
- real,
- double precision,
- smallserial,
- serial,
- bigserial,
- date (ISO 8601 format only),
- timestamp (ISO 8601 format only),
- text,
- boolean.

But, when testing the type validity, it actually do not check the value with the lower and upper
bound of the type. This means the insertion into the database could still fail.

The application is doing a lot of access to the database, which could cause severe performance
problem when trying to insert big volume of data.

When trying to insert or extract big volume of data at once, you may exceed the maximum packet size
or time out. This is because multi part file are not supported.

## Choices

### PostgreSQL

I have chosen the PostgreSQL database for two reasons:
- it supports both document based and relational database,
- it scales rather well compared to other alternative.

### Nginx and uWSGI

I have chosen nginx instead of another web server because it is simpler than Apache to configure
and has better performances.
Since nginx does not have support for the wsgi application, I needed a middle ware to allow it to
communicate with my application. I chose uWSGI because it is commonly used in pair with nginx.

### Bottle

I choose this micro framework because it is very light (it is only one file), and provide enough
functionality for this kind of application.

## How to launch

This application has been made to run with docker. To run it, simply do:
```
$ docker-compose build
```
to build the docker images, then:
```
$ docker-compose up
```
to run it.

By default, it will expose:
- port 5432 for the postgreSQL database,
- port 3031 for the uWSGI server,
- port 9090 for the application.
Use this last port to interact with the application.

## Examples

Here are a few examples on how to interact with the application using the python request module.

### Creating a new schema
```python
import request as r

from json import dumps

r.post(
    "http://localhost:9090/naissance",
    json=dumps(dict(id="int", prenom="text", nom="text", date="date"))
)
```

### Insert CSV data into a schema
```python
import request as r

with open('some_file.csv', 'rb') as fcsv:
    r.put(
        "http://localhost:9090/naissance",
        data=fcsv,
        headers={ 'content-type':'application/csv' }
    )
```

### Insert JSON data into a schema
```python
import request as r

from json import dumps

r.put(
    "http://localhost:9090/naissance",
    json=dumps(
        dict(id=10, prenom="Antoine", nom="Dupont", date="2020-12-17"),
        dict(id=11, prenom="Jean", nom="Dupond", date="2019-10-17"),
    )
)
```

### Get CSV data
```python
import request as r

from io import StringIO
from pandas import read_csv

read_csv(
    StringIO(
        r.get(
            "http://localhost:9090/naissance",
            headers={'content-type':'application/csv'}
        ).content.decode()
    )
)
```

### Get JSON data
```python
import request as r

from io import StringIO
from pandas import read_json

read_json(
    StringIO(
        r.get(
            "http://localhost:9090/naissance",
            headers={'content-type':'application/json'}
        ).content.decode()
    )
)
```

### List all available schema
```python
import request as r

from json import loads

loads(
    r.get("http://localhost:9090/").content.decode()
)
```

### Delete a schema
```python
import request as r

r.delete("http://localhost:9090/naissance")
```
